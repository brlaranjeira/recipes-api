package com.brlaranjeira.recipes.recipes.services;

import static org.assertj.core.api.Assertions.*;

import com.brlaranjeira.recipes.recipes.model.User;
import com.brlaranjeira.recipes.recipes.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class UserServiceTest {

    @Mock private UserRepository userRepository;
    @InjectMocks private UserService underTest;

    @Test
    void shouldSaveSameUser() {
        //given
        User user = new User();

        //when
        underTest.save(user);

        //then
        ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);
        verify(userRepository).save(userArgumentCaptor.capture());
        User capturedUser = userArgumentCaptor.getValue();
        assertThat(capturedUser).isEqualTo(user);
    }

    @Test
    void findByEmail() {
        //given
        String email = "jdoe@mailprovider.com";
        when(userRepository.findByEmail(anyString())).thenReturn(new User());
        //when
        underTest.findByEmail(email);
        //then
        verify(userRepository).findByEmail(email);
    }
}