package com.brlaranjeira.recipes.recipes.repository;

import com.brlaranjeira.recipes.recipes.model.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.assertj.core.api.Assertions.*;

import static org.junit.jupiter.api.Assertions.*;


@DataJpaTest
class UserRepositoryTest {

    @Autowired
    private UserRepository underTest;

    private String name = "John Doe";
    private String email = "jd@mailprovider.com";

    @AfterEach
    private void tearDown() {
        underTest.deleteAll();
    }

    @Test
    public void shouldFindByEmail() {
        //given
        User user = new User(name,email);
        underTest.save(user);

        //when
        User found = underTest.findByEmail(email);

        //then
        assertThat(found.getName()).isEqualTo(name);
    }

    @Test
    public void shouldNotFindByEmail() {
        //given
        //no user inserted

        //when
        User found = underTest.findByEmail(email);

        //then
        assertThat(found).isNull();
    }
}