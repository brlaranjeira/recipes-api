package com.brlaranjeira.recipes.recipes.repository;

import com.brlaranjeira.recipes.recipes.model.Recipe;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class RecipeRepositoryTest {

    @Autowired
    private RecipeRepository underTest;

    @AfterEach
    void tearDown() {
        underTest.deleteAll();
    }

    @Test
    void findByTitleContainingIgnoreCase() {
        //given
        List<String> titles = Arrays.asList("Chocolate Cake", "fried egg", "ICE CREAM", "white chocolate");
        titles.stream().forEach( title -> {
            Recipe recipe = new Recipe();
            recipe.setTitle(title);
            underTest.save(recipe);
        });

        //when
        List<Recipe> foundRecipes = underTest.findByTitleContainingIgnoreCase("chocolate");

        //then
        assertThat(foundRecipes).hasSize(2);



    }
}