package com.brlaranjeira.recipes.recipes.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public User(String name , String email) {
        this.name = name;
        this.email = email;
    }

    @Column
    private String name;

    @Column
    private String email;

}
