package com.brlaranjeira.recipes.recipes.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="recipe")
public class Step {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String description;

    @Column(name="step_order")
    private Integer stepOrder;

    @JoinColumn(name = "recipe_id")
    @ManyToOne
    private Recipe recipe;

    @ManyToOne
    @JoinColumn(name = "recipe_ingredient_id")
    private RecipeIngredient recipeIngredient;

}
