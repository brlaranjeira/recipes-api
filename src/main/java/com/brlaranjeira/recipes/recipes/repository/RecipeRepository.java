package com.brlaranjeira.recipes.recipes.repository;

import com.brlaranjeira.recipes.recipes.model.Recipe;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RecipeRepository extends CrudRepository<Recipe,Long> {

    public List<Recipe> findByTitleContainingIgnoreCase(String title);

}
