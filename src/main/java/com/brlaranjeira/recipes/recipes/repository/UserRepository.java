package com.brlaranjeira.recipes.recipes.repository;

import com.brlaranjeira.recipes.recipes.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User,Long> {

    public User findByEmail(String email);

}
