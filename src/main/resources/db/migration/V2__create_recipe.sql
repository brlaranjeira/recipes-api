create table recipe (
    id bigint not null auto_increment,
    title varchar(100) not null,
    user_id bigint,
    primary key (id),
    foreign key (user_id) references user (id) on update cascade on delete set null
);

create table ingredient (
    id bigint not null auto_increment,
    name varchar(100) not null,
    primary key (id)
);

create table measurement_unit (
    id bigint not null auto_increment,
    name varchar(100) not null,
    primary key (id)
);

create table recipe_ingredient (
    id bigint not null auto_increment,
    recipe_id bigint not null,
    ingredient_id bigint not null,
    measurement_unit_id bigint not null,
    quantity float not null,
    primary key (id),
    foreign key (recipe_id) references recipe(id) on delete cascade on update cascade,
    foreign key (ingredient_id) references ingredient(id) on delete restrict on update cascade,
    foreign key (measurement_unit_id) references measurement_unit(id) on delete restrict on update cascade
);

create table step (
    id bigint not null auto_increment,
    description text not null,
    step_order integer not null,
    recipe_id bigint not null,
    recipe_ingredient_id bigint not null,
    primary key (id),
    foreign key (recipe_id) references recipe(id) on delete cascade on update cascade,
    foreign key (recipe_ingredient_id) references recipe_ingredient(id) on delete restrict on update cascade
);